﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplicativo_Console.Models
{
    public class Livro
    {
        public Guid LivroId { get; set; }
        public string Nome { get; set; }
        public Decimal Preco { get; set; }
        public char Edicao { get; set; }
        public long Curtidas { get; set; }
        public DateTime DataCadas { get; set; }
        public int NumPaginas { get; set; }
        public float Dimencoes { get; set; }
        public double Peso { get; set; }
        public bool Seminovo { get; set; }
    }
}
