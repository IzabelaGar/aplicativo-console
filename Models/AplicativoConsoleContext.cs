﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplicativo_Console.Models
{
    class AplicativoConsoleContext : DbContext
    {
        public AplicativoConsoleContext()
            : base("ConnAppConsole")
        {

        }
        public DbSet<Livro> Livros { get; set; }
    }
}
