namespace Aplicativo_Console.Migrations
{
    using Aplicativo_Console.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Aplicativo_Console.Models.AplicativoConsoleContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(Aplicativo_Console.Models.AplicativoConsoleContext context)
        {

        }
    }
}
