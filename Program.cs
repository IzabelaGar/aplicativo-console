﻿using Aplicativo_Console.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplicativo_Console
{
    class Program
    {
        private static AplicativoConsoleContext _context = new AplicativoConsoleContext();

        static void Main(string[] args)
        {
            var obj1Livro = new Livro();
            for (int i = 0; i < 100; i++)
            {
                Console.WriteLine("inserindo");

                //var dateString = "5/1/2008 8:30:52 AM";
                obj1Livro.LivroId = Guid.NewGuid();
                obj1Livro.Nome = "Watchman " + i.ToString();
                obj1Livro.NumPaginas = 170 + i;
                obj1Livro.Peso = 235.068;
                obj1Livro.Preco = 50;
                obj1Livro.Seminovo = false;
                obj1Livro.Edicao = '2';
                obj1Livro.Dimencoes = 12.5F;
                obj1Livro.DataCadas = new DateTime(2008,05,01, 8,30,52); // DateTime.Parse(dateString, System.Globalization.CultureInfo.InvariantCulture);
                obj1Livro.Curtidas = 400 * i;

                _context.Livros.Add(obj1Livro);

            }

            _context.SaveChanges();

            Console.WriteLine("select");
            var livroId = obj1Livro.LivroId;
            var livro = _context.Livros.Find(livroId);
            livro.Curtidas += 1;

            var livro2 = _context.Livros
                           .Where(w => w.Nome == "Watchman" && w.Curtidas > 300)
                           .ToList();


            Console.WriteLine("update");
            _context.Entry(livro).State = EntityState.Modified;
            _context.SaveChanges();



            Console.WriteLine("delete");
            _context.Livros.Remove(livro);
            _context.SaveChanges();


        }
    }
}
